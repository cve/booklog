#!/usr/bin/env perl

use strict;
use warnings;

sub query {
	my ($dump, $sql) = @_;

	# Fetch the data from the database.
	my $cmd = "sqlite3 -cmd \".read $dump\" :memory: \"$sql\"";
	my $csv = `$cmd`;

	# Convert the CSV result to internal data
	my @result;
	my @lines = split(/\n/, $csv);
	foreach (@lines) {
		my $line = $_;
		my @fields = split(/\|/, $line);
		push(@result, \@fields);
	}

	return @result;

}

sub fetch {
	my ($dump) = @_;
	my $sql = 'SELECT r.date, b.isbn, b.title, b.author, b.category, b.language
FROM read AS r
INNER JOIN book AS b
ON r.isbn = b.isbn
ORDER BY r.date ASC;';
	my @csv = query($dump, $sql);

	my @result;
	foreach (@csv) {
		my @cells = @{$_};

		my %hash = ('date' => $cells[0],
			    'isbn' => $cells[1],
			    'title' => $cells[2],
			    'author' => $cells[3],
			    'category' => $cells[4],
			    'language' => $cells[5]
			   );
		push(@result, \%hash);
	}

	return @result
}

my @booklog = fetch("booklog.sql");

my $header = '<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://emilengler.com/index.css">
		<title>Booklog</title>
	</head>
	<body>
		<h1>Booklog</h1>
		<p>
			I am a life-long learner, so books obviously play a very important role in my life.
			Therefore, this list contains all books I have ever read after the 12th of December 2022.
		</p>
		<table border="1">
			<thead>
				<tr>
					<th>date</th>
					<th>isbn</th>
					<th>title</th>
					<th>author</th>
					<th>category</th>
					<th>language</th>
				</tr>
			</thead>
			<tbody>
';
my $footer = '
			</tbody>
	        </table>
	</body>
</html>
';

print $header;
foreach (@booklog) {
	my %cells = %{$_};
	my $row = '
				<tr>
					<td>' . $cells{'date'} . '</td>
					<td>' . $cells{'isbn'} . '</td>
					<td>' . $cells{'title'} . '</td>
					<td>' . $cells{'author'} . '</td>
					<td>' . $cells{'category'} . '</td>
					<td>' . $cells{'language'} . '</td>
				</tr>
';
	print $row;
}
print $footer;
